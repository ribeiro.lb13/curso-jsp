package filter;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import Connection.SingleConnection;


/**
 * ADICIONEI ESSE TRECHO NO WEB.XML
 *  <filter>
  	<filter-name>filter</filter-name>
  	<filter-class>filter.Filter</filter-class>
  </filter>
 * @author Lucas
 *
 */
//ESSA Anota��o substitui a configura��o no XML, ou uso uma ou outra
@WebFilter(urlPatterns = {"/*"})//toda requisi��o passar� pelo filter
public class Filter implements javax.servlet.Filter{
	private static Connection connection;
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			chain.doFilter(request, response);
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
			
		}		
		
	}
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		connection = SingleConnection.getConnection();
		
	}
	


}
