package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Connection.SingleConnection;
import beans.BeanCursoJsp;

public class DaoUsuario {
	private Connection connection;

	public DaoUsuario() {
		connection = SingleConnection.getConnection();
	}

	public void salvar(BeanCursoJsp usuario) {
		try {
			String sql = "insert into usuario(login, senha, nome, telefone, cep, rua, bairro, cidade, estado, ibge, fotobase64, contenttype, curriculoBase64, contentTypeCurriculo, fotoBase64Miniatura, ativo, sexo, perfil)"
					+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, usuario.getLogin());
			statement.setString(2, usuario.getSenha());
			statement.setString(3, usuario.getNome());
			statement.setString(4, usuario.getTelefone());
			statement.setString(5, usuario.getCep());
			statement.setString(6, usuario.getRua());
			statement.setString(7, usuario.getBairro());
			statement.setString(8, usuario.getCidade());
			statement.setString(9, usuario.getEstado());
			statement.setString(10, usuario.getIbge());
			statement.setString(11, usuario.getFotoBase64());
			statement.setString(12, usuario.getContentType());
			statement.setString(13, usuario.getCurriculoBase64());
			statement.setString(14, usuario.getContentTypeCurriculo());
			statement.setString(15, usuario.getFotoBase64Miniatura());
			statement.setBoolean(16, usuario.isAtivo());
			statement.setString(17, usuario.getSexo());
			statement.setString(18, usuario.getPerfil());

			statement.execute();
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				connection.rollback();

			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	public List<BeanCursoJsp> listar(String descricaoconsulta) throws SQLException{
		String sql = "Select * from usuario where login <> 'admin' and nome like '%"+descricaoconsulta+"%'";
		return consultarUsuarios(sql);	
	}

	public List<BeanCursoJsp> listar() throws Exception {
		String sql = "Select * from usuario where login <> 'admin' order by id desc ";
		return consultarUsuarios(sql);
	}

	private List<BeanCursoJsp> consultarUsuarios(String sql) throws SQLException {
		List<BeanCursoJsp> listar = new ArrayList<>();
		PreparedStatement statement = connection.prepareStatement(sql);
		ResultSet resultSet = statement.executeQuery();
		while (resultSet.next()) {
			BeanCursoJsp beanCursoJsp = new BeanCursoJsp();
			beanCursoJsp.setId(resultSet.getLong("id"));
			beanCursoJsp.setLogin(resultSet.getString("login"));
			beanCursoJsp.setSenha(resultSet.getString("senha"));
			beanCursoJsp.setNome(resultSet.getString("nome"));
			beanCursoJsp.setTelefone(resultSet.getString("telefone"));
			beanCursoJsp.setCep(resultSet.getString("cep"));
			beanCursoJsp.setRua(resultSet.getString("rua"));
			beanCursoJsp.setBairro(resultSet.getString("bairro"));
			beanCursoJsp.setCidade(resultSet.getString("cidade"));
			beanCursoJsp.setEstado(resultSet.getString("estado"));
			beanCursoJsp.setIbge(resultSet.getString("ibge"));
			// beanCursoJsp.setFotoBase64(resultSet.getString("fotobase64"));
			beanCursoJsp.setFotoBase64Miniatura(resultSet.getString("fotobase64miniatura"));
			beanCursoJsp.setContentType(resultSet.getString("contenttype"));
			beanCursoJsp.setCurriculoBase64(resultSet.getString("curriculoBase64"));
			beanCursoJsp.setContentTypeCurriculo(resultSet.getString("contentTypeCurriculo"));
			beanCursoJsp.setAtivo(resultSet.getBoolean("ativo"));
			beanCursoJsp.setSexo(resultSet.getString("sexo"));
			beanCursoJsp.setPerfil(resultSet.getString("perfil"));
			listar.add(beanCursoJsp);

		}
		return listar;
	}

	public void deletar(String id) {

		try {
			String sql = "DELETE FROM usuario WHERE id = '" + id + "'and login <> 'admin'";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.execute();
			connection.commit();

		} catch (Exception e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (Exception e2) {
				e.printStackTrace();
			}
		}
	}

	public boolean validarLogin(String login) throws Exception {
		// false = EXISTE
		// true = N�O EXISTE
		String sql = "SELECT count(1) as qtd FROM usuario WHERE login = '" + login + "'";

		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		ResultSet resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {

			return resultSet.getInt("qtd") <= 0;
		}

		return false;
	}

	public boolean validarSenha(String senha) throws Exception {
		// false = EXISTE
		// true = N�O EXISTE
		String sql = "SELECT count(1) as qtd FROM usuario WHERE senha = '" + senha + "'";

		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		ResultSet resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {

			return resultSet.getInt("qtd") <= 0;
		}

		return false;
	}

	public boolean validarLoginUpdate(String login, Long id) throws Exception {
		// false = EXISTE
		// true = N�O EXISTE
		String sql = "SELECT count(1) as qtd FROM usuario WHERE login = '" + login + "' and id <> " + id;

		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		ResultSet resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {

			return resultSet.getInt("qtd") <= 0;
		}

		return false;
	}

	public boolean validarSenhaUpdate(String senha, Long id) throws Exception {
		// false = EXISTE
		// true = N�O EXISTE
		String sql = "SELECT count(1) as qtd FROM usuario WHERE senha = '" + senha + "' and id <> " + id;

		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		ResultSet resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {

			return resultSet.getInt("qtd") <= 0;
		}

		return false;
	}

	public void atualizar(BeanCursoJsp usuario) {
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE usuario SET login = ?, senha = ?, nome = ?, ")
					.append("cep = ?, rua = ?, bairro = ?, cidade = ?, estado = ?, ibge = ?");

			if (usuario.isAtualizarImage()) {
				sql.append(", fotoBase64 = ?, contentType = ?, fotoBase64Miniatura = ?");
			}

			if (usuario.isAtualizarCurriculo()) {
				sql.append(", curriculoBase64 = ?, contentTypeCurriculo = ?");
			}

			sql.append(", ativo = ? , sexo = ?, perfil = ?").append(" WHERE id = " + usuario.getId());

			try (PreparedStatement statement = connection.prepareStatement(sql.toString())) {

				int index = 1;

				statement.setString(index++, usuario.getLogin());
				statement.setString(index++, usuario.getSenha());
				statement.setString(index++, usuario.getNome());
				statement.setString(index++, usuario.getCep());
				statement.setString(index++, usuario.getRua());
				statement.setString(index++, usuario.getBairro());
				statement.setString(index++, usuario.getCidade());
				statement.setString(index++, usuario.getEstado());
				statement.setString(index++, usuario.getIbge());

				if (usuario.isAtualizarImage()) {
					statement.setString(index++, usuario.getFotoBase64());
					statement.setString(index++, usuario.getContentType());
					statement.setString(index++, usuario.getFotoBase64Miniatura());
				}

				if (usuario.isAtualizarCurriculo()) {
					statement.setString(index++, usuario.getCurriculoBase64());
					statement.setString(index++, usuario.getContentTypeCurriculo());
				}

				statement.setBoolean(index++, usuario.isAtivo());
				statement.setString(index++, usuario.getSexo());
				statement.setString(index++, usuario.getPerfil());
				statement.executeUpdate();
			}
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	public BeanCursoJsp consultar(String id) throws Exception {
		String sql = "SELECT * FROM usuario WHERE id = '" + id + "'and login <> 'admin'";

		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		ResultSet resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			BeanCursoJsp beanCursoJsp = new BeanCursoJsp();
			beanCursoJsp.setId(resultSet.getLong("id"));
			beanCursoJsp.setLogin(resultSet.getString("login"));
			beanCursoJsp.setSenha(resultSet.getString("senha"));
			beanCursoJsp.setNome(resultSet.getString("nome"));
			beanCursoJsp.setTelefone(resultSet.getString("telefone"));
			beanCursoJsp.setCep(resultSet.getString("cep"));
			beanCursoJsp.setRua(resultSet.getString("rua"));
			beanCursoJsp.setBairro(resultSet.getString("bairro"));
			beanCursoJsp.setCidade(resultSet.getString("cidade"));
			beanCursoJsp.setEstado(resultSet.getString("estado"));
			beanCursoJsp.setIbge(resultSet.getString("ibge"));
			beanCursoJsp.setFotoBase64(resultSet.getString("fotobase64"));
			beanCursoJsp.setFotoBase64Miniatura(resultSet.getString("fotobase64miniatura"));
			beanCursoJsp.setContentType(resultSet.getString("contenttype"));
			beanCursoJsp.setCurriculoBase64(resultSet.getString("curriculoBase64"));
			beanCursoJsp.setContentTypeCurriculo(resultSet.getString("contentTypeCurriculo"));
			beanCursoJsp.setAtivo(resultSet.getBoolean("ativo"));
			beanCursoJsp.setSexo(resultSet.getString("sexo"));
			beanCursoJsp.setPerfil(resultSet.getString("perfil"));

			return beanCursoJsp;
		}

		return null;
	}
}