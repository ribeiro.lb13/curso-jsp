package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import Connection.SingleConnection;
import beans.Telefones;

public class DaoTelefones {
	private Connection connection;

	public DaoTelefones() {
		connection = SingleConnection.getConnection();
	}

	public void salvar(Telefones telefones) {
		try {
			String sql = "insert into telefones(numero, tipo, usuario) values (?, ?, ?)";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, telefones.getNumero());
			statement.setString(2, telefones.getTipo());
			statement.setLong(3, telefones.getUsuario());
			statement.execute();
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				connection.rollback();

			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	public List<Telefones> listar(Long user) throws Exception {
		List<Telefones> listar = new ArrayList<>();

		String sql = "Select * from telefones where usuario = ?";
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setLong(1, user);
		ResultSet resultSet = statement.executeQuery();
		while (resultSet.next()) {
			Telefones telefones = new Telefones();
			telefones.setId(resultSet.getLong("id"));
			telefones.setNumero(resultSet.getString("numero"));
			telefones.setTipo(resultSet.getString("tipo"));
			telefones.setUsuario(resultSet.getLong("usuario"));

			listar.add(telefones);
		}

		return listar;
	}

	public void deletar(String id) {

		try {
			String sql = "DELETE FROM telefones WHERE id = '" + id + "'";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.execute();
			connection.commit();

		} catch (Exception e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (Exception e2) {
				e.printStackTrace();
			}
		}
	}

}