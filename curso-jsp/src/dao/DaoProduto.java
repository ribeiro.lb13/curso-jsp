package dao;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.swing.text.DefaultEditorKit.InsertBreakAction;

import Connection.SingleConnection;
import beans.BeanCategoria;
import beans.BeanCursoJsp;
import beans.Produto;

public class DaoProduto {
	private Connection connection;

	public DaoProduto() {
		connection = SingleConnection.getConnection();
	}

	public void salvar(Produto usuario) {
		try {
			String sql = "insert into produto(nome, quantidade, valor, categoria_id) values (?, ?, ?, ?)";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, usuario.getNome());
			statement.setDouble(2, usuario.getQuantidade());
			statement.setDouble(3, usuario.getValor());
			statement.setLong(4, usuario.getCategoria_id());
			statement.execute();
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				connection.rollback();

			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	public List<Produto> listar() throws Exception {
		List<Produto> listar = new ArrayList<>();
		
		String sql = "Select * from produto order by id desc ";
		PreparedStatement statement = connection.prepareStatement(sql);
		ResultSet resultSet = statement.executeQuery();
		while (resultSet.next()) {
			Produto produto = new Produto();
			produto.setId(resultSet.getLong("id"));
			produto.setNome(resultSet.getString("nome"));
			produto.setQuantidade(resultSet.getDouble("quantidade"));
			produto.setValor(resultSet.getDouble("valor"));
			produto.setCategoria_id(resultSet.getLong("categoria_id"));

			listar.add(produto);
		}

		return listar;
	}
	
	public List<BeanCategoria> listaCategoria() throws Exception {
		List<BeanCategoria> listar = new ArrayList<>();	
		
		String sql = "Select * from categoria";
		PreparedStatement statement = connection.prepareStatement(sql);
		ResultSet resultSet = statement.executeQuery();
		while (resultSet.next()) {
			BeanCategoria beanCategoria = new BeanCategoria();
			beanCategoria.setId(resultSet.getLong("id"));
			beanCategoria.setNome(resultSet.getString("nome"));
			
			listar.add(beanCategoria);
		}

		return listar;
	}
	
	public void deletar(String id) {
		
		try {
			String  sql = "DELETE FROM produto WHERE id = '"+id+"'";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.execute();
			connection.commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (Exception e2) {
				e.printStackTrace();
			}
		}
	}

	public boolean validarNome(String nome) throws Exception{
		//false = EXISTE
		//true = N�O EXISTE
		String sql = "SELECT count(1) as qtd FROM produto WHERE nome = '" + nome +"'";
		
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
			
				
				return resultSet.getInt("qtd") <= 0;
			}
		
		return false;
	}
	
	
	public boolean validarNomeUpdate(String nome, Long id) throws Exception{
		//false = EXISTE
		//true = N�O EXISTE
		String sql = "SELECT count(1) as qtd FROM produto WHERE nome = '" + nome +"' and id <> "+id;
		
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
			
				
				return resultSet.getInt("qtd") <= 0;
			}
		
		return false;
	}
	

	public void atualizar(Produto produto) {
		String sql = "update produto set nome = ?, quantidade = ?, valor = ? , categoria_id = ? where id = "+produto.getId();
		
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, produto.getNome());
			statement.setDouble(2, produto.getQuantidade());
			statement.setDouble(3, produto.getValor());
			statement.setLong(4, produto.getCategoria_id());
			statement.executeUpdate();
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		
	}
	
	public Produto consultar(String id) throws Exception{
		String sql = "SELECT * FROM produto WHERE id = '" + id +"'";
		
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				Produto produto = new Produto();
				produto.setId(resultSet.getLong("id"));
				produto.setNome(resultSet.getString("nome"));
				produto.setQuantidade(resultSet.getDouble("quantidade"));
				produto.setValor(resultSet.getDouble("valor"));
				produto.setCategoria_id(resultSet.getLong("categoria_id"));
				return produto;				
			}
		
		return null;
	}
}