package servlet;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.bind.DatatypeConverter;
import javax.xml.datatype.*;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.codec.binary.Base64;

import beans.BeanCursoJsp;
import dao.DaoUsuario;

@WebServlet("/salvarUsuario")
@MultipartConfig
public class Usuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DaoUsuario daoUsuario = new DaoUsuario();

	public Usuario() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String acao = request.getParameter("acao") != null ? request.getParameter("acao") : "listartodos";
			String user = request.getParameter("user");

			if (acao != null && acao.equalsIgnoreCase("delete") && user != null) {
				daoUsuario.deletar(user);
				RequestDispatcher vewDispatcher = request.getRequestDispatcher("/cadastroUsuario.jsp");
				request.setAttribute("usuarios", daoUsuario.listar());
				vewDispatcher.forward(request, response);
			} else if (acao != null && acao.equalsIgnoreCase("editar")) {
				BeanCursoJsp beanCursoJsp = daoUsuario.consultar(user);
				daoUsuario.atualizar(beanCursoJsp);
				RequestDispatcher vewDispatcher = request.getRequestDispatcher("/cadastroUsuario.jsp");
				request.setAttribute("user", beanCursoJsp);
				vewDispatcher.forward(request, response);
			} else if (acao != null && acao.equalsIgnoreCase("listartodos")) {
				RequestDispatcher vewDispatcher = request.getRequestDispatcher("/cadastroUsuario.jsp");
				request.setAttribute("usuarios", daoUsuario.listar());

				vewDispatcher.forward(request, response);

				// fazer dowload autom�tico da imagem
			} else if (acao != null && acao.equalsIgnoreCase("dowload")) {
				BeanCursoJsp usuario = daoUsuario.consultar(user);
				if (usuario != null) {
					String contentType = "";
					byte[] fileBytes = null;
					String tipo = request.getParameter("tipo");

					if (tipo.equalsIgnoreCase("image")) {
						contentType = usuario.getContentType();
						fileBytes = new Base64().decodeBase64(usuario.getFotoBase64());
					} else if (tipo.equalsIgnoreCase("curriculo")) {
						contentType = usuario.getContentTypeCurriculo();
						fileBytes = new Base64().decodeBase64(usuario.getCurriculoBase64());
					}
					// setando imagem no cabe�alho para ficar dinamico
					response.setHeader("Content-Disposition",
							"attachment;filename=arquivo." + contentType.split("\\/")[1]);

					// converte a base 64 do banco para byte[]
					// fileBytes = new Base64().decodeBase64(usuario.getFotoBase64());

					OutputStream oStream = response.getOutputStream();
					oStream.write(fileBytes);

					/*
					 * codico alex // colocar os bytes em um objeto de entrada para processar
					 * java.io.InputStream is = new ByteArrayInputStream(imageFotoBytes);
					 * 
					 * //inicio da resposta para o navegador int read = 0; byte[] bytes = new
					 * byte[1024]; OutputStream os = response.getOutputStream(); while((read =
					 * is.read(bytes)) != -1) { os.write(bytes, 0, read); }
					 */
					oStream.flush();
					oStream.close();

				}
			} else {
				RequestDispatcher vewDispatcher = request.getRequestDispatcher("/cadastroUsuario.jsp");
				request.setAttribute("usuarios", daoUsuario.listar());
				vewDispatcher.forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String acao = request.getParameter("acao");

		if (acao != null && acao.equalsIgnoreCase("reset")) {
			try {
				RequestDispatcher view = request.getRequestDispatcher("/cadastroUsuario.jsp");
				request.setAttribute("usuarios", daoUsuario.listar());
				view.forward(request, response);

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			String id = request.getParameter("id");
			String login = request.getParameter("login");
			String senha = request.getParameter("senha");
			String nome = request.getParameter("nome");
			String telefone = request.getParameter("telefone");
			String cep = request.getParameter("cep");
			String rua = request.getParameter("rua");
			String bairro = request.getParameter("bairro");
			String cidade = request.getParameter("cidade");
			String estado = request.getParameter("estado");
			String ibge = request.getParameter("ibge");
			String sexo = request.getParameter("sexo");
			String perfil = request.getParameter("perfil");

			BeanCursoJsp usuario = new BeanCursoJsp();
			usuario.setId(!id.isEmpty() ? Long.parseLong(id) : null);
			usuario.setLogin(login);
			usuario.setSenha(senha);
			usuario.setNome(nome);
			usuario.setTelefone(telefone);
			usuario.setCep(cep);
			usuario.setRua(rua);
			usuario.setBairro(bairro);
			usuario.setCidade(cidade);
			usuario.setEstado(estado);
			usuario.setIbge(ibge);
			usuario.setSexo(sexo);
			usuario.setPerfil(perfil);
	

			
			if (request.getParameter("ativo") != null && request.getParameter("ativo").equalsIgnoreCase("on")) {
				usuario.setAtivo(true);
			} else {
				usuario.setAtivo(false);
			}

			boolean mantemDadosEmTela = false;
			try {

				/* INICIO file UploadContext deContext imagems e pdf */
				if (ServletFileUpload.isMultipartContent(request)) {

					Part imagemFotoPart = request.getPart("foto");
					// NOVO ARQUIVO
					if (imagemFotoPart != null && imagemFotoPart.getInputStream().available() > 0) {

						String fotobase64 = new Base64()
								.encodeBase64String(converteStreamParaByte(imagemFotoPart.getInputStream()));
						usuario.setFotoBase64(fotobase64);
						usuario.setContentType(imagemFotoPart.getContentType());

						/* Inicio miniatura imagem */

						/* Transforma em bufferedImage */
						byte[] imageByteDecode = new Base64().decodeBase64(fotobase64);
						BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(imageByteDecode));

						/* Pega o tipo da imagem */
						int type = bufferedImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : bufferedImage.getType();

						/* cria imagem em miniatura */
						BufferedImage resizedImage = new BufferedImage(100, 100, type);
						Graphics2D g = resizedImage.createGraphics();
						g.drawImage(bufferedImage, 0, 0, 100, 100, null);
						g.dispose();

						/* Escrever imagem novamente */
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						ImageIO.write(resizedImage, "png", baos);

						String miniaturaBase64 = "data:imagem/png;base64,"
								+ DatatypeConverter.printBase64Binary(baos.toByteArray());

						usuario.setFotoBase64Miniatura(miniaturaBase64);
						/* fim miniatura imagem */
					} else {// UPDATE
						usuario.setAtualizarImage(false);

					}

					/*
					 * List<FileItem> fileItems = new ServletFileUpload(new
					 * DiskFileItemFactory()).parseRequest(request); for (FileItem fileItem :
					 * fileItems) { if(fileItem.getFieldName().equalsIgnoreCase("foto")) { String
					 * fotoBase64 = new Base64().encodeBase64String(fileItem.get()); String
					 * contetType = fileItem.getContentType(); usuario.setFotoBase64(fotoBase64);
					 * usuario.setContentType(contetType);
					 * 
					 * } }
					 */
//						processa Pdf 
					Part curriculoPdf = request.getPart("curriculo");

					if (curriculoPdf != null && curriculoPdf.getInputStream().available() > 0) {
						String curriculobase64 = new Base64()
								.encodeBase64String(converteStreamParaByte(curriculoPdf.getInputStream()));
						usuario.setCurriculoBase64(curriculobase64);
						usuario.setContentTypeCurriculo(curriculoPdf.getContentType());
					} else {
						usuario.setAtualizarCurriculo(false);
					}

				}

				/* FIM file UploadContext deContext imagems e pdf */

				// NOVO
				if (id == null || id.isEmpty()) {
					if (login.isEmpty()) {
						request.setAttribute("msg", "Login deve ser informado!");
						mantemDadosEmTela = true;
					} else if (senha.isEmpty()) {
						request.setAttribute("msg", "Senha deve ser informado!");
						mantemDadosEmTela = true;
					} else if (nome.isEmpty()) {
						request.setAttribute("msg", "Nome deve ser informado!");
						mantemDadosEmTela = true;
					} else if (telefone.isEmpty()) {
						request.setAttribute("msg", "Telefone deve ser informado!");
						mantemDadosEmTela = true;
					} else if (daoUsuario.validarLogin(login) && daoUsuario.validarSenha(senha)) {
						daoUsuario.salvar(usuario);
						request.setAttribute("msg", "Usuário cadastrado com sucesso!");
					} else if (!daoUsuario.validarLogin(login) && !daoUsuario.validarSenha(senha)) {
						mantemDadosEmTela = true;
						request.setAttribute("msg", "Login e Senha j� existe para outro usu�rio!");
					} else if (!daoUsuario.validarLogin(login)) {
						request.setAttribute("msg", "Login j� existe para outro usu�rio!");
						mantemDadosEmTela = true;
					} else {
						request.setAttribute("msg", "Senha j� existe para outro usu�rio!");
						mantemDadosEmTela = true;
					}
					// UPDATE
				} else {
					if (login.isEmpty()) {
						request.setAttribute("msg", "Login deve ser informado!");
						mantemDadosEmTela = true;
					} else if (senha.isEmpty()) {
						request.setAttribute("msg", "Senha deve ser informado!");
						mantemDadosEmTela = true;
					} else if (nome.isEmpty()) {
						request.setAttribute("msg", "Nome deve ser informado!");
						mantemDadosEmTela = true;
					} else if (telefone.isEmpty()) {
						request.setAttribute("msg", "Telefone deve ser informado!");
						mantemDadosEmTela = true;
					} else if (daoUsuario.validarLoginUpdate(login, Long.parseLong(id))
							&& daoUsuario.validarSenhaUpdate(senha, Long.parseLong(id))) {
						daoUsuario.atualizar(usuario);
						request.setAttribute("msg", "Usu�rio editado com sucesso!");
					} else if (!daoUsuario.validarLoginUpdate(login, Long.parseLong(id))
							&& !daoUsuario.validarSenhaUpdate(senha, Long.parseLong(id))) {
						request.setAttribute("msg", "Login e Senha j� existe para outro usu�rio!");
						mantemDadosEmTela = true;
					} else if (!daoUsuario.validarLoginUpdate(login, Long.parseLong(id))) {
						request.setAttribute("msg", "Login j� existe para outro usu�rio!");
						mantemDadosEmTela = true;
					} else {
						request.setAttribute("msg", "Senha j� existe para outro usu�rio!");
						mantemDadosEmTela = true;
					}
				}
				if (mantemDadosEmTela) {
					request.setAttribute("user", usuario);
				}

//				// minha l�gica
//				if (!daoUsuario.validarLogin(login)) {
//					
//					//if(id.equals(""));
//					if (id.isEmpty()) {
//						daoUsuario.salvar(usuario);
//					} else {
//						if (!daoUsuario.validarLoginUpdate(login, Long.parseLong(id))) {
//							request.setAttribute("msg", "Login j� existe para outro usu�rio!");
//						}else if(!daoUsuario.validarSenhaUpdate(senha, Long.parseLong(id))) {
//							request.setAttribute("msg", "Senha j� existe para outro usu�rio!");
//						}
//						else {
//							daoUsuario.atualizar(usuario);
//					}
//					}
//				}else{
//					request.setAttribute("msg", "Usu�rio j� existe com o mesmo login!");
//			}
//				if (id == null || id.isEmpty() && !daoUsuario.validarLogin(login)) {
//					request.setAttribute("msg", "Usu�rio j� existe com o mesmo login!");
//				}
//				if (id == null || id.isEmpty() && daoUsuario.validarLogin(login)) {
//					daoUsuario.salvar(usuario);
//				} else if ((id != null || !id.isEmpty())) {
//					if (!daoUsuario.validarUpdate(login, Long.parseLong(id))) {
//						request.setAttribute("msg", "Login j� existe para outro usu�rio!");
//					} else {
//						daoUsuario.atualizar(usuario);
//					}
//				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				RequestDispatcher view = request.getRequestDispatcher("/cadastroUsuario.jsp");
				request.setAttribute("usuarios", daoUsuario.listar());
				view.forward(request, response);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	/* converte a entrada de fluxo de dados da imagem para um byte[] */
	private byte[] converteStreamParaByte(java.io.InputStream inputStream) throws IOException {

		byte[] baoStream = IOUtils.toByteArray(inputStream);
		return baoStream;

		// m�todo do alex
//		ByteArrayOutputStream baoStream = new ByteArrayOutputStream();
//		int reads = inputStream.read();
//		while(reads != -1) {
//			baoStream.write(reads);
//			reads = inputStream.read();
//		}		
//		return baoStream.toByteArray();		
	}

}
