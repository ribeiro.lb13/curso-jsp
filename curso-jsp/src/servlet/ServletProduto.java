package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Produto;
import beans.Produto;
import dao.DaoProduto;
import dao.DaoUsuario;

@WebServlet("/salvarProduto")
public class ServletProduto extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DaoProduto daoProduto = new DaoProduto();

	public ServletProduto() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String acao = request.getParameter("acao") != null ? request.getParameter("acao") : "listartodos";
			String user = request.getParameter("user");

			RequestDispatcher vewDispatcher = request.getRequestDispatcher("/cadastroProduto.jsp");
			if (acao != null && user != null && acao.equalsIgnoreCase("delete")) {
				daoProduto.deletar(user);
				request.setAttribute("usuarios", daoProduto.listar());
			} else if (acao != null && acao.equalsIgnoreCase("editar")) {
				Produto produto = daoProduto.consultar(user);
				daoProduto.atualizar(produto);
				request.setAttribute("user", produto);
			} else if (acao != null && acao.equalsIgnoreCase("listartodos")) {
				request.setAttribute("usuarios", daoProduto.listar());

			}else {
				request.setAttribute("usuarios", daoProduto.listar());
				vewDispatcher.forward(request, response);
			}
			request.setAttribute("categoria", daoProduto.listaCategoria());
			vewDispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String acao = request.getParameter("acao");

		if (acao != null && acao.equalsIgnoreCase("reset")) {
			try {
				RequestDispatcher view = request.getRequestDispatcher("/cadastroProduto.jsp");
				request.setAttribute("usuarios", daoProduto.listar());
				view.forward(request, response);

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			String id = request.getParameter("id");
			String nome = request.getParameter("nome");
			String quantidade = request.getParameter("quantidade");
			String categoria = request.getParameter("categoria_id");

			String valor = request.getParameter("valor");
			String valorProduto = valor.replaceAll("\\.", "");// 10.500,20
			valorProduto = valorProduto.replaceAll("\\,", ".");// 10500.20

			Produto usuario = new Produto();
			usuario.setId(!id.isEmpty() ? Long.parseLong(id) : null);
			usuario.setNome(nome);
			usuario.setQuantidade(!quantidade.isEmpty() ? Double.parseDouble(quantidade) : 0);
			usuario.setValor(Double.parseDouble(valorProduto));
			usuario.setCategoria_id(Long.parseLong(categoria));
			boolean mantemDadosEmTela = false;
			boolean produtoValido = false;
			boolean quantidadeValida = false;
			boolean valorValido = false;
			if (!quantidade.isEmpty()) {
				if (Double.parseDouble(quantidade) > 0)
					quantidadeValida = true;
			}
			if (!valor.isEmpty()) {
				if (Double.parseDouble(valorProduto) > 0)
					valorValido = true;
			}
			if (quantidadeValida && valorValido) {
				produtoValido = true;
			}
			try {

				// NOVO
				if (id == null || id.isEmpty()) {
					if (nome.isEmpty()) {
						request.setAttribute("msg", "Nome deve ser informado!");
						mantemDadosEmTela = true;
					} else if (quantidade.isEmpty()) {
						request.setAttribute("msg", "Quantidade deve ser informado!");
						mantemDadosEmTela = true;
					} else if (valor.isEmpty()) {
						request.setAttribute("msg", "Valor deve ser informado!");
						mantemDadosEmTela = true;
					} else if (!produtoValido) {
						request.setAttribute("msg", "Uma quantidade e valor validos devem ser informados!");
						mantemDadosEmTela = true;

					} else if (!quantidadeValida) {
						request.setAttribute("msg", "Uma quantidade valida deve ser informada");
						mantemDadosEmTela = true;

					} else if (!valorValido) {
						request.setAttribute("msg", "Um valor valido deve ser informada!");
						mantemDadosEmTela = true;

					} else if (daoProduto.validarNome(nome) && !nome.isEmpty()) {
						daoProduto.salvar(usuario);
						request.setAttribute("msg", "Produto cadastrado com sucesso!");
					} else if (!daoProduto.validarNome(nome)) {
						request.setAttribute("msg", "Cadastro j� existe para outro Produto!");
						mantemDadosEmTela = true;
						// UPDATE
					}
				} else {
					if (nome.isEmpty()) {
						request.setAttribute("msg", "Nome deve ser informado!");
						mantemDadosEmTela = true;
					} else if (quantidade.isEmpty()) {
						request.setAttribute("msg", "Quantidade deve ser informado!");
						mantemDadosEmTela = true;
					} else if (valor.isEmpty()) {
						request.setAttribute("msg", "Valor deve ser informado!");
						mantemDadosEmTela = true;
					} else if (!produtoValido) {
						request.setAttribute("msg", "Uma quantidade e valor validos devem ser informados!");
						mantemDadosEmTela = true;

					} else if (!quantidadeValida) {
						request.setAttribute("msg", "Uma quantidade valida deve ser informada");
						mantemDadosEmTela = true;

					} else if (!valorValido) {
						request.setAttribute("msg", "Um valor valido deve ser informada!");
						mantemDadosEmTela = true;

					} else if (daoProduto.validarNomeUpdate(nome, Long.parseLong(id)) && produtoValido) {
						daoProduto.atualizar(usuario);
						request.setAttribute("msg", "Produto editado com sucesso!");
					} else if (!daoProduto.validarNomeUpdate(nome, Long.parseLong(id))) {
						request.setAttribute("msg", "Cadastro j� existe para outro Produto!");
						mantemDadosEmTela = true;

					}
				}
				if (mantemDadosEmTela) {
					request.setAttribute("user", usuario);
				}

//				// minha l�gica
//				if (!daoUsuario.validarLogin(login)) {
//					
//					//if(id.equals(""));
//					if (id.isEmpty()) {
//						daoUsuario.salvar(usuario);
//					} else {
//						if (!daoUsuario.validarLoginUpdate(login, Long.parseLong(id))) {
//							request.setAttribute("msg", "Login j� existe para outro usu�rio!");
//						}else if(!daoUsuario.validarSenhaUpdate(senha, Long.parseLong(id))) {
//							request.setAttribute("msg", "Senha j� existe para outro usu�rio!");
//						}
//						else {
//							daoUsuario.atualizar(usuario);
//					}
//					}
//				}else{
//					request.setAttribute("msg", "Usu�rio j� existe com o mesmo login!");
//			}
//				if (id == null || id.isEmpty() && !daoUsuario.validarLogin(login)) {
//					request.setAttribute("msg", "Usu�rio j� existe com o mesmo login!");
//				}
//				if (id == null || id.isEmpty() && daoUsuario.validarLogin(login)) {
//					daoUsuario.salvar(usuario);
//				} else if ((id != null || !id.isEmpty())) {
//					if (!daoUsuario.validarUpdate(login, Long.parseLong(id))) {
//						request.setAttribute("msg", "Login j� existe para outro usu�rio!");
//					} else {
//						daoUsuario.atualizar(usuario);
//					}
//				}

			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				RequestDispatcher view = request.getRequestDispatcher("/cadastroProduto.jsp");
				request.setAttribute("usuarios", daoProduto.listar());
				request.setAttribute("categoria", daoProduto.listaCategoria());
				view.forward(request, response);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
