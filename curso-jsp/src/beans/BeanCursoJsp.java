package beans;

public class BeanCursoJsp {
	private Long id;
	private String login;
	private String nome;
	private String senha;
	private String telefone;
	private String cep;
	private String rua;
	private String bairro;
	private String cidade;
	private String estado;
	private String ibge;
	private String fotoBase64;
	private String fotoBase64Miniatura;
	private String curriculoBase64;
	private String contentTypeCurriculo;
	private String contentType;
	private String tempFotoUser;
	private boolean ativo;
	private String sexo;
	private String perfil;
	
	
	private boolean atualizarImage = true;
	private boolean atualizarCurriculo = true;
	
	
	public boolean isAtualizarCurriculo() {
		return atualizarCurriculo;
	}

	public void setAtualizarCurriculo(boolean atualizarCurriculo) {
		this.atualizarCurriculo = atualizarCurriculo;
	}

	public boolean isAtualizarImage() {
		return atualizarImage;
	}

	public void setAtualizarImage(boolean atualizarImage) {
		this.atualizarImage = atualizarImage;
	}



	public String getFotoBase64Miniatura() {
		return this.fotoBase64Miniatura;
	}

	public void setFotoBase64Miniatura(String fotoBase64Miniatura) {
		this.fotoBase64Miniatura = fotoBase64Miniatura;
	}

	public String getCurriculoBase64() {
		return curriculoBase64;
	}

	public void setCurriculoBase64(String curriculoBase64) {
		this.curriculoBase64 = curriculoBase64;
	}

	public String getContentTypeCurriculo() {
		return contentTypeCurriculo;
	}

	public void setContentTypeCurriculo(String contentTypeCurriculo) {
		this.contentTypeCurriculo = contentTypeCurriculo;
	}

	public void setTempFotoUser(String tempFotoUser) {
		this.tempFotoUser = tempFotoUser;
	}

	public String getTempFotoUser() {
		tempFotoUser = "data:"+contentType+";base64," + fotoBase64;
		return tempFotoUser;
	}
	
	public String getFotoBase64() {
		return fotoBase64;
	}


	public void setFotoBase64(String fotoBase64) {
		this.fotoBase64 = fotoBase64;
	}


	public String getContentType() {
		return contentType;
	}


	public void setContentType(String contentType) {
		this.contentType = contentType;
	}


	
	

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getSenha() {
		return senha;
	}


	public void setSenha(String senha) {
		this.senha = senha;
	}


	public String getTelefone() {
		return telefone;
	}


	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}


	public String getCep() {
		return cep;
	}


	public void setCep(String cep) {
		this.cep = cep;
	}


	public String getRua() {
		return rua;
	}


	public void setRua(String rua) {
		this.rua = rua;
	}


	public String getBairro() {
		return bairro;
	}


	public void setBairro(String bairro) {
		this.bairro = bairro;
	}


	public String getCidade() {
		return cidade;
	}


	public void setCidade(String cidade) {
		this.cidade = cidade;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getIbge() {
		return ibge;
	}


	public void setIbge(String ibge) {
		this.ibge = ibge;
	}

	@Override
	public String toString() {
		return "BeanCursoJsp [id=" + id + ", login=" + login + ", nome=" + nome + ", senha=" + senha + ", telefone="
				+ telefone + ", cep=" + cep + ", rua=" + rua + ", bairro=" + bairro + ", cidade=" + cidade + ", estado="
				+ estado + ", ibge=" + ibge + ", fotoBase64=" + fotoBase64 + ", fotoBase64Miniatura="
				+ fotoBase64Miniatura + ", curriculoBase64=" + curriculoBase64 + ", contentTypeCurriculo="
				+ contentTypeCurriculo + ", contentType=" + contentType + ", tempFotoUser=" + tempFotoUser + "]";
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}






}
