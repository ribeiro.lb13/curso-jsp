<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="ISO-8859-1">
<title>Cadastro Produto</title>
<script src="resources/javaScript/jquery.min.js" type="text/javascript"></script>
<script src="resources/javaScript/jquery.maskMoney.min.js"
	type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
	href="resources/css/cadastrar.css">
</head>

<body>
	<a href="acessoliberado.jsp"><img alt="Inicio" title="Inicio"
		src="resources/img/home.png" width="30px" height="30px"></a>
	<a href="index.jsp"><img alt="sair" title="sair"
		src="resources/img/logout.png" width="30px" height="30px"></a>
	<center>
		<h1>Cadastro de Produto</h1>
		<h3 style="color: orange;">${msg}</h3>
	</center>
	<form action="salvarProduto" method="post" id="formUser"
		onsubmit="return validarCampos() ? true : false;">
		<ul class="form-style-1">
			<li>
				<table>
					<caption>Produtos cadastrados</caption>
					<tr>
						<td>C�digo:</td>
						<td><input type="text" readonly="readonly" id="id" name="id"
							value="${user.id}" class="field-long" maxlength="100"></td>
					</tr>
					<tr>
						<td>Nome:</td>
						<td><input type="text" id="nome" name="nome"
							value="${user.nome}" class="field-long" maxlength="100"></td>
					</tr>
					<tr>
						<td>Quantidade:</td>
						<td><input type="text" id="quantidade" name="quantidade"
							value="${user.quantidade}" class="field-long" maxlength="7"></td>
					</tr>

					<tr>
						<td>Valor R$:</td>
						<td><input type="text" data-thousands="." data-decimal=","
							id="valor" name="valor" value="${user.valorEmTexto}"
							maxlength="8" class="field-long"></td>

					</tr>
					<tr>
						<td>Categoria:</td>
						<td><select id="categorias" name="categoria_id">
								<c:forEach items="${categoria}" var="cat">
									<option value="${cat.id}" id="${cat.id}"
										<c:if test="${cat.id == user.categoria_id}">
									<c:out value = "selected=selected"/>
									</c:if>>
									${cat.nome}</option>
								</c:forEach>
						</select></td>
					</tr>

					<tr>
						<td></td>
						<td><input type="submit" name="salvar" value="Salvar">
							<input type="submit" value="cancelar"
							onclick="document.getElementById('formUser').action = 'salvarProduto?acao=reset'"></td>

					</tr>
				</table>
			</li>
		</ul>
		<center>
			<h2>Lista de Produtos</h2>
		</center>
	</form>
	<div class="container">
		<table class="responsive-table">
			<tr>
				<th align="center">Id</th>
				<th>Nome</th>
				<th>Quantidade</th>
				<th>Valor R$</th>
				<th>Deletar</th>
				<th>Editar</th>
			</tr>
			<c:forEach items="${usuarios}" var="user">
				<tr>
					<td><c:out value="${user.id}"></c:out></td>
					<td style="width: 150px"><c:out value="${user.nome}"></c:out></td>
					<td><c:out value="${user.quantidade}"></c:out></td>
					<fmt:setLocale value="es_ES" />
					<td><fmt:formatNumber type="number" maxFractionDigits="3"
							value="${user.valor}" /></td>
					<td><a href="salvarProduto?acao=delete&user=${user.id}"
						onclick="return confirm('Confirmar a exclus�o?');"><img
							src="resources/img/delete.png" width="20px" height="20px"></a></td>
					<td><a href="salvarProduto?acao=editar&user=${user.id}"><img
							src="resources/img/edit.png" width="20px" height="20px"></a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
	<script type="text/javascript">
		function validarCampos() {
			if (document.getElementById("nome").value == '') {
				alert('Informe o Nome');
				return false;
			}
			if (document.getElementById("quantidade").value == '') {
				alert('Informe o Quantidade');
				return false;
			}
			if (document.getElementById("valor").value == '') {
				alert('Informe o Valor');
				return false;
			}
			return true;
		}

		$(function() {
			$('#valor').maskMoney();
		});

		$(document).ready(function() {
			$("#quantidade").keyup(function() {
				$("#quantidade").val(this.value.match(/[0-9]*/));
			});
		});
	</script>
</body>
</html>