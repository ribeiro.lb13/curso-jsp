
<!-- Chama o bean java-->
<jsp:useBean id="calcula" class="beans.BeanCursoJsp" type="beans.BeanCursoJsp" scope="page"/>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    
    pageEncoding="ISO-8859-1"%>
    
    	<!-- incluir na pasta lib de WEB-INF o jstl-1.2.jar e na lib do tomcat -->
        <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><!-- tag para o JSTL -->
	
	
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	
	
	
	
	<!-- Redirecionamento-->
	<c:set var ="teste1" value="51"></c:set>
	<c:if test="${teste > 50}">
		<c:redirect url="https://www.google.com.br"></c:redirect>
	</c:if>
	<c:if test="${teste < 50}">
		<c:redirect url="https://www.uol.com.br"></c:redirect>
	</c:if>
	
	
	
	<br>
	<br>
	<br>	
	<!-- Motando URL-->
	<c:url value="/acessoliberado.jsp" var="acesso">
		<c:param name="para1" value="111"></c:param>
		<c:param name="para2" value="222"></c:param>
	</c:url>
	${acesso}
	
	<br>
	<br>
	<br>	
	
	<!-- Quebrando String por um caracter-->
	<c:forTokens items="Lucas-Ribeiro-Goes" delims="-" var="nome">
		Nome: <c:out value="${nome}"></c:out><br>
	</c:forTokens>	
	
	<br>
	<br>
	<br>
	<!-- La�o for -->
	<c:forEach var="n" begin="1" end="5">
		Item:${n}<br>
	</c:forEach>
		
	<br>
	<br>
	<br>
	<!-- Capturando erro -->
	<c:catch var="erro">
		<%int var = 100/0; %>	
	</c:catch>
	<c:if test="${erro != null}">
		${erro.message}
	</c:if>
	
	
	<br>
	<br>
	<br>
	<!-- condicional -->
	<c:set var="numero" value="${100/2}"></c:set>
	<c:choose>
		<c:when test="${numero >= 50}">
			<c:out value="${'Maior que 50'}"></c:out>
		</c:when>
		
		<c:when test="${numero < 50}">
			<c:out value="${'Menor que 50'}"></c:out>
		</c:when>
		
		<c:otherwise>
			<c:out value="${'N�o encontrou o valor correto!'}"></c:out>
		</c:otherwise>
	</c:choose>
	
	<br>
	<br>
	<br>
	
	
	<c:set var ="data" scope="page" value="${500*6}"/>
	<c:out value="${data}"></c:out>
	<!--<c:out value="${'Bem vindo ao JSTL'}"></c:out> <!-- printa na tela  -->
	<!--<c:import var = "data" url="https://www.google.com.br"></c:import> <!-- Importa documento da url  --> 
	
	
	
	
	<p/>
	<p/>
	<p/>
	<p/>
	<p/>
	<h1>Bem vindo ao curso de JSP</h1>
	<!-- Passando para a servelet-->
	<form action="LoginServlet" method="post">
		Login:
		<input type="text" id="login" name="login">
		<br>
		Senha:
		<input type="text" id="senha" name="senha">
		<br>
		<input type="submit" value="Logar">
	</form>
</body>
</html>