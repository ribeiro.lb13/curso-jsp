<%@page import="beans.BeanCursoJsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cadastro Usu�rio</title>
<link rel="stylesheet" type="text/css"
	href="resources/css/cadastrar.css">
<!-- Adicionando JQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
<title>M�scaras de telefone (Brasil)</title>
<script type='text/javascript'
	src='//code.jquery.com/jquery-compat-git.js'></script>
<script type='text/javascript'
	src='//igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js'></script>
</head>
<body>
	<a href="acessoliberado.jsp"><img alt="Inicio" title="Inicio"
		src="resources/img/home.png" width="30px" height="30px"></a>
	<a href="index.jsp"><img alt="sair" title="sair"
		src="resources/img/logout.png" width="30px" height="30px"></a>

	<center>
		<h1>Cadastro de Usu�rio</h1>
		<h3 style="color: orange;">${msg}</h3>
	</center>
	<form action="salvarUsuario" method="post" id="formUser"
		onsubmit="return validarCampos() ? true : false;"
		enctype="multipart/form-data">
		<ul class="form-style-1">
			<li>
				<table>
					<caption>Usu�rios cadastrados</caption>
					<tr>
						<td>C�digo:</td>
						<td><input type="text" readonly="readonly" id="id" name="id"
							value="${user.id}" class="field-long"></td>

						<td>CEP:</td>
						<td><input type="text" id="cep" name="cep"
							value="${user.cep}" onblur="consultaCep();"></td>
					</tr>
					<tr>
						<td>Login:</td>
						<td><input type="text" id="login" name="login"
							value="${user.login}" class="field-long" maxlength="10"></td>

						<td>Rua:</td>
						<td><input type="text" id="rua" name="rua"
							value="${user.rua}" maxlength="50"></td>
					</tr>
					<tr>
						<td>Senha:</td>
						<td><input type="password" id="senha" name="senha"
							value="${user.senha}" class="field-long" maxlength="10"></td>

						<td>Bairro:</td>
						<td><input type="text" id="bairro" name="bairro"
							value="${user.bairro}" maxlength="50"></td>
					</tr>
					<tr>
						<td>Nome:</td>
						<td><input type="text" id="nome" name="nome"
							value="${user.nome}" class="field-long"
							placeholder="Informe o nome do Usu�rio" maxlength="50"></td>

						<td>Cidade:</td>
						<td><input type="text" id="cidade" name="cidade"
							value="${user.cidade}" maxlength="50"></td>
					</tr>
					<tr>
						<td>Telefone:</td>
						<td><input type="text" id="telefone"
							placeholder="(21) 12345-6789" name="telefone"
							value="${user.telefone}" class="field-long"></td>

						<td>Estado:</td>
						<td><input type="text" id="estado" name="estado"
							value="${user.estado}"></td>
					</tr>

					<tr>
						<td>IBGE:</td>
						<td><input type="text" id="ibge" name="ibge"
							value="${user.ibge}" class="field-long"></td>

						<td>Ativo:</td>
						<td><input type="checkbox" id="ativo" name="ativo"
							<%if (request.getAttribute("user") != null) {
				BeanCursoJsp user = (BeanCursoJsp) request.getAttribute("user");
				if (user.isAtivo()) {
					out.print(" ");
					out.print("checked=\"checked\"");
					out.print(" ");
				}
			}%>>

						</td>
					<tr>
						<td>Perfil:</td>
						<td><select id="perfil" name="perfil"
							style="width: 185px; height: 35px;">
								<option value="n�o_informado">[--SELECIONE--]</option>
								<option value="administrador"
									${user.perfil eq 'administrador' ? 'selected' : '' }>Administrador</option>
								<option value="secretario"
									${user.perfil eq 'secretario' ? 'selected' : '' }>Secret�rio(a)</option>
								<option value="gerente"
									${user.perfil eq 'gerente' ? 'selected' : '' }>Gerente</option>
								<option value="funcionario"
									${user.perfil eq 'funcionario' ? 'selected' : '' }>Funcion�rio</option>
						</select></td>
						<td>Curr�culo</td>
						<td><input type="file" name="curriculo" value="curriculo"
							style="font-size: 12px"></td>


					</tr>

					<tr>
						<td>Sexo:</td>
						<td><input type="radio" name="sexo" value="masculino"${user.sexo eq 'masculino' ? 'checked' : '' }
				<%-- C�DIGO ALEX			<%if (request.getAttribute("user") != null) {
				BeanCursoJsp user = (BeanCursoJsp) request.getAttribute("user");
				if (user.getSexo().equalsIgnoreCase("masculino")) {
					out.print(" ");
					out.print("checked=\"checked\"");
					out.print(" ");
				}
			}%> --%>>Masculino
							<input type="radio" name="sexo" value="feminino"${user.sexo eq 'feminino' ? 'checked' : '' }
							
					<%-- C�DIGO ALEX		<%if (request.getAttribute("user") != null) {
				BeanCursoJsp user = (BeanCursoJsp) request.getAttribute("user");
				if (user.getSexo().equalsIgnoreCase("feminino")) {
					out.print(" ");
					out.print("checked=\"checked\"");
					out.print(" ");
				}
			}%> --%>>feminino</td>
						<td>Foto</td>
						<td><input type="file" name="foto" style="font-size: 12px">
					</tr>


					<tr>
						<td></td>

						<td><input type="submit" name="salvar" value="Salvar">
							<input type="submit" value="cancelar"
							onclick="document.getElementById('formUser').action = 'salvarUsuario?acao=reset'"></td>

					</tr>
				</table>
			</li>
		</ul>
		
		</form>
	<form method="post" action="servletpesquisa">
		<ul class="form-style-1">
			<li>
				<table>
					<tr>
						<td>Descri��o</td>
						<td><input type="text" id="descricaoconsulta"
							name="descricaoconsulta"></td>
						<td><input type="submit" value="pesquisar"></td>
					</tr>
				</table>
			</li>
		</ul>
	</form>
		<center>
			<h2>Lista de Usu�rios</h2>
		</center>
	

	<div class="container">
		<table class="responsive-table">
			<tr>
				<th align="center">Id</th>
				<th>Fotinha</th>
				<th>PDF</th>
				<th>Nome</th>
				<th>Deletar</th>
				<th>Editar</th>
				<th>Telefone</th>
			</tr>
			<c:forEach items="${usuarios}" var="user">
				<tr>
					<td><c:out value="${user.id}"></c:out></td>
					<c:if test="${user.fotoBase64Miniatura.isEmpty() == false}">
						<td><a
							href="salvarUsuario?acao=dowload&tipo=image&user=${user.id}">
								<img src='<c:out value="${user.fotoBase64Miniatura}"></c:out>'
								alt="Imagem User" title="Imagem User"
								style="border-radius: 30px" width="50px" height="50px">
						</a></td>
					</c:if>
					<c:if test="${user.fotoBase64Miniatura == null}">
						<td><img src="resources/img/user.png" alt="Imagem User"
							title="Imagem User" style="border-radius: 30px" width="50px"
							height="50px" onclick="alert('N�o Possui Imagem!')"></td>
					</c:if>

					<c:if test="${user.curriculoBase64.isEmpty() == false}">
						<td><a
							href="salvarUsuario?acao=dowload&tipo=curriculo&user=${user.id}">
								<img style="width: 50px" height="50px" alt="Curriculo"
								src="resources/img/pdf.png">
						</a></td>

					</c:if>
					<c:if
						test="${user.curriculoBase64 == null || user.curriculoBase64.isEmpty() }">
						<td><img style="width: 50px" height="50px" alt="Curriculo"
							src="resources/img/pdf-file-symbol.png"
							onclick="alert('N�o Possui Curr�culo!')"></td>

					</c:if>



					<td><c:out value="${user.nome}"></c:out></td>

					<td><a href="salvarUsuario?acao=delete&user=${user.id}"
						onclick="return confirm('Confirmar a exclus�o?');"><img
							alt="Deletar" title="Deletar" src="resources/img/delete.png"
							width="20px" height="20px"></a></td>

					<td><a href="salvarUsuario?acao=editar&user=${user.id}"><img
							alt="Editar" title="Editar" src="resources/img/edit.png"
							width="20px" height="20px"></a></td>

					<td><a href="salvarTelefone?&acao=addfone&user=${user.id}"><img
							alt="Telefones" title="Telefones" src="resources/img/phone.png"
							width="20px" height="20px"></a></td>
				</tr>
			</c:forEach>
		</table>
	</div>

	<script type="text/javascript">
		function validarCampos() {
			if (document.getElementById("login").value == '') {
				alert('Informe o Login');
				return false;
			}
			if (document.getElementById("senha").value == '') {
				alert('Informe o Senha');
				return false;
			}
			if (document.getElementById("nome").value == '') {
				alert('Informe o Nome');
				return false;
			}
			if (document.getElementById("telefone").value == '') {
				alert('Informe o Telefone');
				return false;
			}
			return true;
		}

		function consultaCep() {
			var cep = $("#cep").val();

			//Consulta o webservice viacep.com.br/
			$.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?",
					function(dados) {

						if (!("erro" in dados)) {
							$("#rua").val(dados.logradouro);
							$("#bairro").val(dados.bairro);
							$("#cidade").val(dados.localidade);
							$("#estado").val(dados.uf);
							$("#ibge").val(dados.ibge);

						} //end if.
						else {
							$("#rua").val('');
							$("#bairro").val('');
							$("#cidade").val('');
							$("#estado").val('');
							$("#ibge").val('');
							alert("CEP n�o encontrado.");
						}
					});
		}
		var behavior = function(val) {
			return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000'
					: '(00) 0000-00009';
		}, options = {
			onKeyPress : function(val, e, field, options) {
				field.mask(behavior.apply({}, arguments), options);
			}
		};

		$('#telefone').mask(behavior, options);
	</script>
</body>
</html>