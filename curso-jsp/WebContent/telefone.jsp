<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cadastro Telefone</title>
<link rel="stylesheet" type="text/css"
	href="resources/css/cadastrar.css">
<!-- Adicionando JQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
	
	<head>
	<!-- Font Awesome -->
	<link href="library/fontawesome/5.0.13/web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet">
	
    <!-- Bootstrap -->
	<link href="library/css/bootstrap.min.css" rel="stylesheet" />
	<link href="library/css/cesf.css" rel="stylesheet"/>

	<!-- JS -->
	<script src="library/js/jquery-3.2.1.js" type="text/javascript"></script>
	
	<title>M�scaras de telefone (Brasil)</title>
    <script type='text/javascript' src='//code.jquery.com/jquery-compat-git.js'></script>
    <script type='text/javascript' src='//igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js'></script>
</head>
<body>
		<a href="acessoliberado.jsp"><img alt="Inicio" title="Inicio"
		src="resources/img/home.png" width="30px" height="30px"></a>
	<a href="index.jsp"><img alt="sair" title="sair"
		src="resources/img/logout.png" width="30px" height="30px"></a>


	<center>
		<h1>Cadastro de Telefones</h1>
		<h3 style="color: orange;">${msg}</h3>
	</center>
	<form action="salvarTelefone" method="post" id="formUser"
		onsubmit="return validarCampos() ? true : false;">
		<ul class="form-style-1">
			<li>
				<table>
					<caption>Telefones cadastrados</caption>
					<tr>
						<td>User:</td>
						<td><input type="text" readonly="readonly" id="user"
							name="user" value="${userEscolhido.id}" class="field-long"></td>

						<td><input type="text" readonly="readonly" id="nome"
							name="nome" value="${userEscolhido.nome}" class="field-long"></td>


					</tr>
 
					<tr>
						<td>N�mero</td>
						<td><input type="text" id="numero" name="numero" placeholder="(21) 12345-6789" width: 190px">
						<td><select id="tipo" name="tipo" style="width: 190px">
								<option>Casa</option>
								<option>Contato</option>
								<option>Celular</option>
						</select></td>
					</tr>

					<td></td>
					<td><input type="submit" name="salvar" value="Salvar" style="width: 190px">


					</td>
					<td><input type="submit" value="voltar"
						onclick="document.getElementById('formUser').action='salvarTelefones?acao=voltar'" style="width: 190px">


					</td>


				</table>
			</li>
		</ul>
		<center>
			<h2>Lista de Telefones</h2>
		</center>
	</form>
	<div class="container">
		<table class="responsive-table">
			<tr>
				<th align="center">Id</th>
				<th>N�mero</th>
				<th>Tipo</th>
				<th>Excluir</th>
			</tr>
			<c:forEach items="${telefones}" var="telefone">
				<tr>
					<td><c:out value="${telefone.id}"></c:out></td>
					<td style="width: 150px"><c:out value="${telefone.numero}"></c:out></td>
					<td><c:out value="${telefone.tipo}"></c:out></td>

					<td><a
						href="salvarTelefone?user=${telefone.usuario}&acao=deleteFone&idFone=${telefone.id}" onclick="return confirm('Confirmar a exclus�o?');"><img
							src="resources/img/delete.png" width="20px" height="20px"></a></td>

				</tr>
			</c:forEach>
		</table>
	</div>

	<script type="text/javascript">
		function validarCampos() {
			if (document.getElementById("numero").value == '') {
				alert('Informe o n�mero');
				return false;
			}

			if (document.getElementById("tipo").value == '') {
				alert('Informe o Tipo');
				return false;
			}

			return true;
		}

		function consultaCep() {
			var cep = $("#cep").val();

			//Consulta o webservice viacep.com.br/
			$.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?",
					function(dados) {

						if (!("erro" in dados)) {
							$("#rua").val(dados.logradouro);
							$("#bairro").val(dados.bairro);
							$("#cidade").val(dados.localidade);
							$("#estado").val(dados.uf);
							$("#ibge").val(dados.ibge);

						} //end if.
						else {
							$("#rua").val('');
							$("#bairro").val('');
							$("#cidade").val('');
							$("#estado").val('');
							$("#ibge").val('');
							alert("CEP n�o encontrado.");
						}
					});
		}
		
		var behavior = function (val) {
		    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		},
		options = {
		    onKeyPress: function (val, e, field, options) {
		        field.mask(behavior.apply({}, arguments), options);
		    }
		};

		$('#numero').mask(behavior, options);
		
	</script>
</body>
<!-- JS -->
<script src="library/js/jquery-ui-1.12.1.custom.min.js" type="text/javascript"></script>
<!-- JS Plugin Mascara do Formulario -->
<script src="library/js/jquery.mask.min.js" type="text/javascript"></script>
</html>